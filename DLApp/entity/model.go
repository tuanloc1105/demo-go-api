package entity

import (
	"github.com/google/uuid"
	"time"
)

type BaseInfo struct {
	UpdatedAt time.Time
	CreatedAt time.Time
}

type User struct {
	Id        int       `json:"id"`
	Password  string    `json:"password"`
	Username  string    `json:"username"`
	UpdatedAt time.Time `json:"updatedAt"`
	CreatedAt time.Time `json:"createdAt"`
	UserId    uuid.UUID `json:"userId"`
}

type ToDo struct {
	Id        int       `json:"id"`
	ToDoTitle string    `json:"todo_title"`
	ToDoName  string    `json:"todo_name"`
	RemindAt  time.Time `json:"remind_at"`
	UpdatedAt time.Time `json:"created_at"`
	CreatedAt time.Time `json:"updated_at"`
	Active    bool      `json:"active"`
	UserId    int       `json:"user_id"`
}

func GenerateBaseInfo() *BaseInfo {
	var baseInfo BaseInfo
	createdAt, _ := time.Parse(time.Layout, time.Now().Format(time.Layout))
	updatedAt, _ := time.Parse(time.Layout, time.Now().Format(time.Layout))
	baseInfo.UpdatedAt = createdAt
	baseInfo.CreatedAt = updatedAt
	return &baseInfo
}
