package main

import (
	"DLApp/service"
	"github.com/gin-gonic/gin"
)

//var (
//	db *gorm.DB = connect.DbConnectGorm()
//)

func main() {
	r := SetupRoute()
	r.Run(":8080")
}

func SetupRoute() *gin.Engine {
	r := gin.Default()
	auth := r.Group("/api/auth")
	{
		auth.POST("/sign-up", service.SignUp())
		auth.POST("/log-in", service.LogIn())
	}

	client := r.Group("/api/user" /*, middleware.AuthorizeJWT()*/)
	{
		client.PUT("/update", service.UpdatePassword())
	}

	todoclient := r.Group("/api/todo" /*, middleware.AuthorizeJWT()*/)
	{
		todoclient.POST("/create/:id", service.CreateTodo())
		todoclient.GET("/get/:id", service.GetToDo())
		todoclient.GET("/get-all", service.GetAllToDo())
		todoclient.PUT("/update/:id", service.UpdateToDo())
		todoclient.DELETE("/disable/:id", service.DisableToDo())
		todoclient.DELETE("/delete/:id", service.DeleteToDo())
	}

	return r
}
