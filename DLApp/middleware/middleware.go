package middleware

import (
	"DLApp/response"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"os"
	"strings"
	"time"
)

func ValidateToken(token string) (bool, error) {
	jwtKey := []byte(os.Getenv("ACCESS_SECRET"))
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = false
	atClaims["user_id"] = nil
	atClaims["exp"] = nil
	tkn, err := jwt.ParseWithClaims(token, atClaims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return false, err
		}
		return false, err
	}
	if !tkn.Valid {
		return false, errors.New("invalid token")
	}
	return true, nil
}

func AuthorizeJWT() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ok, err := ValidateToken(strings.Split(ctx.GetHeader("Authorization"), " ")[1])
		if !ok {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, response.BuildErrorResponse("unauthorized", err.Error(), nil))
			return
		}
		ctx.AbortWithStatusJSON(http.StatusOK, response.BuildResponse(true, "authorized", nil))
	}
}

func GenerateAccessToken(uuid uuid.UUID) (string, error) {
	jwtKey := []byte(os.Getenv("ACCESS_SECRET"))
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = uuid
	atClaims["exp"] = time.Now().Add(time.Minute * 14400).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString(jwtKey)

	return token, err
}
