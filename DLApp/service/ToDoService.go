package service

import (
	"DLApp/connect"
	"DLApp/entity"
	"DLApp/middleware"
	"DLApp/request"
	"DLApp/response"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func CreateTodo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		db := connect.DbConnect()
		ok, err := middleware.ValidateToken(strings.Split(ctx.GetHeader("Authorization"), " ")[1])
		if !ok {
			ctx.JSON(http.StatusUnauthorized, err.Error())
			return
		}
		createToDoRequest := new(request.CreateToDoRequest)
		if err := ctx.BindJSON(&createToDoRequest); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		baseInfo := entity.GenerateBaseInfo()
		createdAt := baseInfo.CreatedAt
		updatedAt := baseInfo.UpdatedAt
		remindAt, err := request.ParseTimeFromString(createToDoRequest.RemindAt)
		var ToDoInsertedInDB entity.ToDo
		row, err := db.Query(
			"INSERT INTO todo(todo_title, todo_name, remind_at, created_at, updated_at, active, user_id) SELECT $1, $2, $3, $4, $5, $6, id FROM public.user WHERE id = $7 RETURNING id, todo_title, todo_name, remind_at, created_at, updated_at, active, user_id",
			createToDoRequest.ToDoTitle, createToDoRequest.ToDoName, remindAt, createdAt, updatedAt, true, ctx.Param("id"),
		)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		for row.Next() {
			var ToDoInserted entity.ToDo
			if err := row.Scan(&ToDoInserted.Id, &ToDoInserted.ToDoTitle, &ToDoInserted.ToDoName, &ToDoInserted.RemindAt, &ToDoInserted.UpdatedAt, &ToDoInserted.CreatedAt, &ToDoInserted.Active, &ToDoInserted.UserId); err != nil {
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"error": err.Error(),
				})
				return
			}
			ToDoInsertedInDB = ToDoInserted
		}
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, response.BuildResponse(true, "inserted todo", ToDoInsertedInDB))
		defer db.Close()
	}
}

func GetToDo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		db := connect.DbConnect()
		ok, err := middleware.ValidateToken(strings.Split(ctx.GetHeader("Authorization"), " ")[1])
		if !ok {
			ctx.JSON(http.StatusUnauthorized, err.Error())
			return
		}
		var ToDoResult entity.ToDo
		row, err := db.Query(
			"SELECT * FROM todo WHERE id = $1 and active = true",
			ctx.Param("id"),
		)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		for row.Next() {
			var ToDoInDB entity.ToDo
			if err := row.Scan(&ToDoInDB.Id, &ToDoInDB.ToDoTitle, &ToDoInDB.ToDoName, &ToDoInDB.RemindAt, &ToDoInDB.UpdatedAt, &ToDoInDB.CreatedAt, &ToDoInDB.Active, &ToDoInDB.UserId); err != nil {
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"error": err.Error(),
				})
				return
			}
			ToDoResult = ToDoInDB
		}
		ctx.JSON(http.StatusOK, response.BuildResponse(true, "success", ToDoResult))
		defer db.Close()
	}
}

func UpdateToDo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		db := connect.DbConnect()
		ok, err := middleware.ValidateToken(strings.Split(ctx.GetHeader("Authorization"), " ")[1])
		if !ok {
			ctx.JSON(http.StatusUnauthorized, err.Error())
			return
		}
		createToDoRequest := new(request.CreateToDoRequest)
		if err := ctx.BindJSON(&createToDoRequest); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		baseInfo := entity.GenerateBaseInfo()
		updatedAt := baseInfo.UpdatedAt
		remindAt, err := request.ParseTimeFromString(createToDoRequest.RemindAt)
		row, err := db.Query(
			"UPDATE todo SET todo_title = $1, todo_name = $2, remind_at = $3, updated_at = $4 WHERE id = $5 RETURNING id, todo_title, todo_name, remind_at, created_at, updated_at, active, user_id",
			createToDoRequest.ToDoTitle, createToDoRequest.ToDoName, remindAt, updatedAt, ctx.Param("id"),
		)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		var ToDoUpdatedInDB entity.ToDo
		for row.Next() {
			var ToDoUpdated entity.ToDo
			if err := row.Scan(&ToDoUpdated.Id, &ToDoUpdated.ToDoTitle, &ToDoUpdated.ToDoName, &ToDoUpdated.RemindAt, &ToDoUpdated.UpdatedAt, &ToDoUpdated.CreatedAt, &ToDoUpdated.Active, &ToDoUpdated.UserId); err != nil {
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"error": err.Error(),
				})
				return
			}
			ToDoUpdatedInDB = ToDoUpdated
		}

		ctx.JSON(http.StatusOK, response.BuildResponse(true, "updated todo", ToDoUpdatedInDB))
		defer db.Close()
	}
}

func DisableToDo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		db := connect.DbConnect()
		ok, err := middleware.ValidateToken(strings.Split(ctx.GetHeader("Authorization"), " ")[1])
		if !ok {
			ctx.JSON(http.StatusUnauthorized, err.Error())
			return
		}
		createToDoRequest := new(request.CreateToDoRequest)
		if err := ctx.BindJSON(&createToDoRequest); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		_, err = db.Query(
			"UPDATE todo SET active = false WHERE id = $1",
			ctx.Param("id"),
		)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}

		ctx.JSON(http.StatusOK, response.BuildResponse(true, "disabled todo", nil))
		defer db.Close()
	}
}

func GetAllToDo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		db := connect.DbConnect()
		ok, err := middleware.ValidateToken(strings.Split(ctx.GetHeader("Authorization"), " ")[1])
		if !ok {
			ctx.JSON(http.StatusUnauthorized, err.Error())
			return
		}
		row, err := db.Query(
			"SELECT * FROM todo WHERE active = $1",
			ctx.DefaultQuery("active", ""),
		)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		var AllToDo []entity.ToDo
		for row.Next() {
			var ToDoModel entity.ToDo
			if err := row.Scan(&ToDoModel.Id, &ToDoModel.ToDoTitle, &ToDoModel.ToDoName, &ToDoModel.RemindAt, &ToDoModel.UpdatedAt, &ToDoModel.CreatedAt, &ToDoModel.Active, &ToDoModel.UserId); err != nil {
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"error": err.Error(),
				})
				return
			}
			AllToDo = append(AllToDo, ToDoModel)
		}
		ctx.JSON(http.StatusOK, response.BuildResponse(true, "disabled todo", AllToDo))
		defer db.Close()
	}
}

func DeleteToDo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		db := connect.DbConnect()
		ok, err := middleware.ValidateToken(strings.Split(ctx.GetHeader("Authorization"), " ")[1])
		if !ok {
			ctx.JSON(http.StatusUnauthorized, err.Error())
			return
		}
		_, err = db.Query(
			"DELETE FROM todo WHERE id = $1",
			ctx.Param("id"),
		)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		ctx.JSON(http.StatusOK, response.BuildResponse(true, "deleted todo", nil))
	}
}
