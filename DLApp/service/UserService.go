package service

import (
	"DLApp/connect"
	"DLApp/entity"
	"DLApp/middleware"
	"DLApp/request"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"os"
	"strings"
	"time"
)

func SignUp() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		db := connect.DbConnect()
		signupRequest := new(request.SignUpRequest)
		if err := ctx.BindJSON(&signupRequest); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		query, err := db.Prepare("SELECT COUNT(u.*) FROM public.user u WHERE LOWER(u.username) = $1")
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		row, _ := query.Query(strings.ToLower(signupRequest.Username))
		var count int
		for row.Next() {
			if err := row.Scan(&count); err != nil {
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"error": err.Error(),
				})
				return
			}
		}
		if count != 0 {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "this user is already existed",
			})
			return
		}
		insert, err := db.Prepare("INSERT INTO public.user(password, username, created_at, updated_at, user_id) VALUES ($1, $2, $3, $4, $5)")
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
		baseInfo := entity.GenerateBaseInfo()
		password, _ := HashPassword(signupRequest.Password)
		createdAt := baseInfo.CreatedAt
		updatedAt := baseInfo.UpdatedAt
		userId := uuid.New()
		_, err = insert.Exec(password, signupRequest.Username, createdAt, updatedAt, userId)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"message": "Sign up completed"})
		defer db.Close()
	}
}

func GenerateAccessToken(uuid uuid.UUID) (string, error) {
	jwtKey := []byte(os.Getenv("ACCESS_SECRET"))
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = uuid
	atClaims["exp"] = time.Now().Add(time.Minute * 100000).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString(jwtKey)

	return token, err
}

func LogIn() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		db := connect.DbConnect()
		username := ctx.DefaultQuery("username", "")
		password := ctx.DefaultQuery("password", "")
		query, err := db.Prepare("SELECT u.id, u.password, u.username, u.created_at, u.updated_at, u.user_id FROM public.user u WHERE u.username = $1")
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		row, _ := query.Query(strings.ToLower(username))
		user := new(entity.User)
		for row.Next() {
			var Id int
			var Password string
			var Username string
			var UpdatedAt time.Time
			var CreatedAt time.Time
			var UserId uuid.UUID
			if err := row.Scan(&Id, &Password, &Username, &UpdatedAt, &CreatedAt, &UserId); err != nil {
				ctx.JSON(http.StatusInternalServerError, gin.H{
					"error": err.Error(),
				})
				return
			}
			user.Id = Id
			user.Password = Password
			user.Username = Username
			user.UpdatedAt = UpdatedAt
			user.CreatedAt = CreatedAt
			user.UserId = UserId
		}
		if (entity.User{}) == *user {
			ctx.JSON(http.StatusNotFound, gin.H{
				"error": "username is incorrect",
			})
			return
		}
		if !CheckPasswordHash(password, user.Password) {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error() + "\npassword is incorrect"})
			return
		}
		token, err := GenerateAccessToken(user.UserId)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Sign up completed",
			"token":   token,
		})
		defer db.Close()
	}
}

func UpdatePassword() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ok, err := middleware.ValidateToken(strings.Split(ctx.GetHeader("Authorization"), " ")[1])
		if !ok {
			ctx.JSON(http.StatusUnauthorized, err.Error())
			return
		}
		ctx.JSON(http.StatusOK, "ok")
	}
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func HashPassword(password string) (*string, error) {
	encodedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return nil, err
	}
	resultPassword := string(encodedPassword)
	return &resultPassword, err
}
