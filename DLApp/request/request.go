package request

import "time"

type SignUpRequest struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type RegisterRequest struct {
	Name     string `json:"name" form:"name" binding:"required"`
	Email    string `json:"email" form:"email" binding:"required,email" `
	Password string `json:"password" form:"password" binding:"required"`
}

type CreateToDoRequest struct {
	ToDoTitle string `json:"todo_title" form:"todo_title" binding:"required"`
	ToDoName  string `json:"todo_name" form:"todo_name" binding:"required"`
	RemindAt  string `json:"remind_at" form:"remind_at" binding:"required"`
}

func ParseTimeFromString(stringTime string) (time.Time, error) {
	parseTime, err := time.Parse("2006-01-02 15:04", stringTime)
	timeParsed, err := time.Parse(time.Layout, parseTime.Format(time.Layout))
	return timeParsed, err
}
